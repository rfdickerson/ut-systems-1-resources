#include <stdio.h>

extern int binsearch(int *myarray, int x, int ilow, int ihigh);

int myarray[] = {2,5,8,13,47,203};

int main()
{

  int x = 47;
  int val = binsearch(myarray, x, 0, 5);

  printf("Value %d found at %d with value %d\n", x, val, myarray[val]);
  
  return 0;
}
