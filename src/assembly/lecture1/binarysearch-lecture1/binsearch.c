#include <stdio.h>

int myarray[] = {3,5,8,12,55,101,200};

int binsearch(int* myarray, int x, int low, int high)
{
  if (low > high)
    {
      return -1;
    }

  int i = (low + high)/2;
  int mid = myarray[i];

  if (mid > x)
    {
      return binsearch(myarray, x, low, i-1);
    } else if (mid < x)
    {
      return binsearch(myarray, x, i+1, high);
    }

  return i;
    
}

int main()
{
  int index = binsearch(myarray, 12, 0, 6);
  printf("The index for 12 is %d\n", index);
  printf("The index of 101 is %d\n", binsearch(myarray, 101, 0, 6));
  
}
