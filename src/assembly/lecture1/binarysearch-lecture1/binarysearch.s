	.section .data

	.align 4
myarray:
	.long 3
	.long 5
	.long 8
	.long 12
	.long 55
	.long 101
	.long 200

	.section .text
	.globl binsearch
_start:
	pushl $6
	pushl $0
	pushl $101
	pushl $myarray
	call binsearch
	addl $16, %esp

	movl $1, %eax		# send sys_exit to OS
	int $0x80		# interrupt to OS
	
	.type binsearch, @function
binsearch:	
	pushl %ebp
	movl %esp, %ebp		# create new stack frame

	pushl %ebx		# callee save
	pushl %edi
	pushl %esi
	
	movl 8(%ebp), %edi	# edi now has myarray
	movl 12(%ebp), %ebx	# ebx has x
	movl 16(%ebp), %ecx	# ecx has low
	movl 20(%ebp), %edx	# edx has high

	cmpl %edx, %ecx		# compare low and high
	ja .L2

	movl %edx, %eax		# store high in temp
	addl %ecx, %eax		# eax = low + high
	shr %eax		# eax = eax / 2

	movl (%edi, %eax, 4), %esi # mid = myarray[index]

	cmpl %ebx, %esi		# compare mid vs x
	ja .L3
	jb .L4

	jmp .finish
	
.L2:
	movl $-1, %eax
	jmp .finish

.L3:
	decl %eax		# i = i - 1
	pushl %eax		# push i-1
	pushl %ecx		# push low
	pushl %ebx		# push x
	pushl %edi		# push myarray
	call binsearch
	addl $16, %esp
	jmp .finish
.L4:
	incl %eax		# i = i + 1
	pushl %edx		# push high
	pushl %eax		# push i + 1
	pushl %ebx
	pushl %edi
	call binsearch
	addl $16, %esp
	jmp .finish
	
.finish:
	popl %esi
	popl %edi
	popl %ebx
	leave
	ret
	
	
	
	
	
	
