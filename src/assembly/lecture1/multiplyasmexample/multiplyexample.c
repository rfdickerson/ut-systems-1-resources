#include <stdio.h>

extern int mult8(int a) __asm__("_mult8");

int main()
{

  int a = mult8(7);

  printf("The result was %d\n", a);
  
  return 0;
}
