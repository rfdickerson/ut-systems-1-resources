#include <stdio.h>

extern int mult3(int a) __asm__("_mult3");

int main()
{

  int a = mult3(3);
  printf("Result is %d\n", a);
  return 0;
}
