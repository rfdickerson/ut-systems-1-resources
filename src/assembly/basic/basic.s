	.section .data
	
	.section .text			# instruction region
	.global _mult3

	#PURPOSE: This function is used to computer the value  of a
	# 	number multiplied by three
	.type _mult3, @function
_mult3:
	pushl %ebp		# push old stack frame
	movl %esp, %ebp		# set new stack frame to the top of stack
	
	movl 8(%ebp), %eax
	sall %eax		# shift arithmetic shift left
				# addl %eax, %eax
	
	movl %ebp, %esp
	popl %ebp
	ret
	
	
