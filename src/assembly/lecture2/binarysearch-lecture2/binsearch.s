	.section .data

	.align 4
myarray:
	.long 3
	.long 7
	.long 12
	.long 36
	.long 100
	.long 144

	.section .text
	.align 16, 0x90
	.globl binsearch

_start:	
	pushl $5		# ihigh is 5
	pushl $0		# ilow is 0
	pushl $36		# x is 36
	pushl $myarray		# myarray pointer
	call binsearch

	addl $16, %esp
	
	movl $1, %eax		# sys_exit
	int $0x80		# call kernel
	
	.type binsearch, @function
binsearch:
	pushl %ebp		
	movl %esp, %ebp		# create stack

	pushl %edi		# callee save state
	pushl %ebx
	pushl %esi
	
	movl 8(%ebp), %edi	# edi = *myarray
	movl 12(%ebp), %ebx	# ebx = x
	movl 16(%ebp), %ecx	# ecx = ilow
	movl 20(%ebp), %edx	# edx = ihigh

	cmpl %edx, %ecx		# compare ilow vs ihigh
	ja .L2			# jump if ilow > ihigh

	movl %ecx, %eax		# eax = ilow
	addl %edx, %eax		# eax += ihigh
	shr %eax		# eax /= 2

	movl (%edi, %eax, 4), %esi # esi = ma[i]

	cmpl %ebx, %esi		# compare mid vs x
	ja .L3
	jb .L4
	# eax contains index of found element
	jmp .finish		

.L2:				# ilow > ihigh

	movl $-1, %eax		# move -1 to return
	jmp .finish

.L3:				# mid > x
	decl %eax

	pushl %ecx		# caller save state
	pushl %edx

	pushl %eax		# push i-1
	pushl %ecx		# push ilow
	pushl %ebx		# push x
	pushl %edi		# push myarray
	
	call binsearch
	addl $16, %esp		# clearing args
	
	popl %edx		# restoring state
	popl %ecx
	
	jmp .finish
.L4:				# mid < x
	incl %eax

	pushl %ecx		# caller save state
	pushl %edx

	pushl %edx		# pushing ihigh
	pushl %eax		# push i+1
	pushl %ebx
	pushl %edi
	
	call binsearch
	addl $16, %esp		# clear args

	popl %edx		# caller restore state
	popl %ecx
	
	jmp .finish
	
.finish:
	popl %esi		# callee restore state
	popl %ebx
	popl %edi
	
	leave
	ret
