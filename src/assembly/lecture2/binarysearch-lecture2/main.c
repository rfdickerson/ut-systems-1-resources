#include <stdio.h>

extern int binsearch (int* myarray, int x, int ilow, int ihigh);

int myarray[] = {1,2,4,8,16,32,64,128};

int main()
{

  int i,x,d;
  for (i=0;i<7;i++)
    {
      x = myarray[i]; 
      d = binsearch(myarray, x, 0, 7);
      printf("Searched for %d and found %d\n", x, myarray[d]);  
    }

}
