#include <stdio.h>

int myarray[] = {3,7,12,36,100, 144};

int binsearch(int *myarray, int x, int ilow, int ihigh)
{
  if (ilow>ihigh)
    return -1;

  int i = (ilow+ihigh)/2;
  int mid = myarray[i];

  if (mid > x)
    {
      return binsearch(myarray, x, ilow, i-1);
    }
  else if (mid < x)
    {
      return binsearch(myarray, x, i+1, ihigh);
    }

  return i;
}

int main()
{

  int index = binsearch(myarray, 100, 0, 5);
  printf("Index for 100 is %d\n", index);  
}
