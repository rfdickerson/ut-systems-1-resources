	.text
	.file	"binarysearch.c"
	.globl	binsearch
	.align	16, 0x90
	.type	binsearch,@function
binsearch:                              # @binsearch
# BB#0:
	pushl	%ebp
	pushl	%ebx
	pushl	%edi
	pushl	%esi
	movl	32(%esp), %ecx
	movl	28(%esp), %edx
	movl	$-1, %eax
	cmpl	%ecx, %edx
	jg	.LBB0_7
# BB#1:
	movl	24(%esp), %esi
	movl	20(%esp), %edi
	.align	16, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	leal	(%ecx,%edx), %ebp
	movl	%ebp, %ebx
	shrl	$31, %ebx
	addl	%ebp, %ebx
	sarl	%ebx
	cmpl	%esi, (%edi,%ebx,4)
	jle	.LBB0_4
# BB#3:                                 # %tailrecurse
                                        #   in Loop: Header=BB0_2 Depth=1
	decl	%ebx
	cmpl	%ebx, %edx
	movl	%ebx, %ecx
	jle	.LBB0_2
	jmp	.LBB0_7
.LBB0_4:                                #   in Loop: Header=BB0_2 Depth=1
	jge	.LBB0_5
# BB#6:                                 # %tailrecurse.outer
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	%ecx, %ebx
	leal	1(%ebx), %edx
	jl	.LBB0_2
	jmp	.LBB0_7
.LBB0_5:
	movl	%ebx, %eax
.LBB0_7:                                # %.loopexit
	popl	%esi
	popl	%edi
	popl	%ebx
	popl	%ebp
	retl
.Ltmp0:
	.size	binsearch, .Ltmp0-binsearch

	.globl	main
	.align	16, 0x90
	.type	main,@function
main:                                   # @main
# BB#0:
	pushl	%esi
	subl	$8, %esp
	xorl	%ecx, %ecx
	movl	$5, %eax
	.align	16, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	leal	(%eax,%ecx), %esi
	movl	%esi, %edx
	shrl	$31, %edx
	addl	%esi, %edx
	sarl	%edx
	movl	myarray(,%edx,4), %esi
	cmpl	$101, %esi
	jl	.LBB1_3
# BB#2:                                 # %tailrecurse.i
                                        #   in Loop: Header=BB1_1 Depth=1
	decl	%edx
	movl	$-1, %esi
	cmpl	%edx, %ecx
	movl	%edx, %eax
	jle	.LBB1_1
	jmp	.LBB1_6
.LBB1_3:                                #   in Loop: Header=BB1_1 Depth=1
	cmpl	$99, %esi
	jg	.LBB1_4
# BB#5:                                 # %tailrecurse.outer.i
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	$-1, %esi
	cmpl	%eax, %edx
	leal	1(%edx), %ecx
	jl	.LBB1_1
	jmp	.LBB1_6
.LBB1_4:
	movl	%edx, %esi
.LBB1_6:                                # %binsearch.exit
	movl	%esi, 4(%esp)
	movl	$.L.str, (%esp)
	calll	printf
	xorl	%eax, %eax
	addl	$8, %esp
	popl	%esi
	retl
.Ltmp1:
	.size	main, .Ltmp1-main

	.type	myarray,@object         # @myarray
	.data
	.globl	myarray
	.align	4
myarray:
	.long	3                       # 0x3
	.long	7                       # 0x7
	.long	12                      # 0xc
	.long	36                      # 0x24
	.long	100                     # 0x64
	.long	144                     # 0x90
	.size	myarray, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Index for 100 is %d\n"
	.size	.L.str, 21


	.ident	"clang version 3.6.0 (trunk 225634)"
	.section	".note.GNU-stack","",@progbits
