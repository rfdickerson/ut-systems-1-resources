	# PROJECT: multiply by four

	.section .data

	.section .text
	.global _mult4

_mult4:
	pushl %ebp
	movl %esp, %ebp

	movl 8(%ebp), %eax
	sall $2, %eax

	movl %ebp, %esp
	popl %ebp
	ret
