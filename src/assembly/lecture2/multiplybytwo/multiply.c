#include <stdio.h>

extern int mult4(int a) __asm__("_mult4");

int main()
{
  int a = mult4(8);

  printf("The result was %d\n", a);
  return 0;
}
