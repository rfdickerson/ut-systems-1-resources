#include <stdio.h>
#include <stdlib.h>

enum Type {
  BUILDING,
  UNIT
};

enum UnitType {
  PROBE,
  ZEALOT,
  STALKER,
  SENTRY,
  IMMORTAL
};

typedef struct {

  float health;
  
} Building;

typedef struct {

  enum UnitType type;
  float health;
  float attack;
  float movement; 
  
} Unit;

typedef struct NodeElement {
  enum Type eType;

  union {
    Building building;
    Unit unit;
  };
    
  struct NodeElement* next;

} Node;

typedef struct {
  Node* head; // first element in the list
  Node* tail; // last element in the list
} LinkedList;

void init_building_node(Node* n, float health)
{

  n->eType = BUILDING;
  n->building.health = health;
  n->next = NULL;
}

void init_unit_node(Node* n, enum UnitType type, float health, float attack)
{
  n->eType = UNIT;
  n->unit.health = health;
  n->unit.attack = attack;
  n->unit.type = type;
  n->next = NULL;
}

void display_info(Node *node)
{
  if (node == NULL)
    {
      printf("Node is uninitialized!\n");
      return;
    }
  
  if (node->eType == BUILDING)
    {
      printf("Building with %f health\n", node->building.health);
    }
  else {
    printf("Unit with %f health and %f attack\n",
	   node->unit.health, node->unit.attack);
    
  }
}

void display_list(LinkedList* myList)
{
  Node* currentNode;
  
  currentNode = myList->head;
  
  while (currentNode) {
    display_info(currentNode);
    currentNode = currentNode->next;
  }
}

void add_to_list(LinkedList* myList, Node* myNode)
{
  if (myList->head && myList->tail) {
    myList->tail->next = myNode;
    myList->tail = myNode;
  } else {
    myList->head = myNode;
    myList->tail = myNode;
  }
}



int main()
{
  Node *n1;
  char input;
  int running = 1;

  LinkedList myList;
  myList.head = NULL;
  myList.tail = NULL;
  
  while (running) {
    printf("\nb: build Pylon, u: make Zealot, l: list units, q: quit\n");
    printf("--------------------------------------------------------\n");
    printf("Action: ");
    scanf(" %c", &input);
    switch (input)
      {
      case 'q':
	running = 0;
	break;
      case 'b':
	printf("Created a Pylon\n");
	n1 = (Node*)malloc(sizeof(Node));
	init_building_node(n1, 1.00);
      
	add_to_list(&myList, n1); 
	break;
      case 'w':
	printf("Warped in a Zealot\n");
	n1 = (Node*)malloc(sizeof(Node));
	init_unit_node(n1, ZEALOT, 1.00, 5.00);
	add_to_list(&myList, n1);
	break;
      case 'l':
	display_list(&myList);
	break;
      default:
	printf("Did not respond correctly, try again\n");
      }
    
  }
        
  free(n1);

  // free(n3);
}
