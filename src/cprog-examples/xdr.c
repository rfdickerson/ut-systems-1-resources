/**
 * XDR vulnerability example (page 91)
 **/
 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void* copy_elements(void *ele_src[], int ele_cnt, size_t ele_size)
{
  void *result = malloc(ele_cnt * ele_size);
  if (result == NULL)
    return NULL;
  void *next = result;
  int i;
  for (i=0 ; i<ele_src[i] ; i++)
    {
      memcpy(next, ele_src[i], ele_size);
      next += ele_size;
    }

  return result;
};

int main()
{

  int a[] = {2,4,6,8,10,12,14,16,18};

  void* newmemory;
  newmemory = copy_elements( (void*)&a, 9, 4);
  
  return 0;
}
  
