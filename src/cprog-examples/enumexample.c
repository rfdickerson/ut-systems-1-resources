#include <stdio.h>


enum cardsuit {
  CLUBS = 1,
  DIAMONDS = 2,
  HEARTS = 3,
  SPADES = 8
};


int main()
{

  enum cardsuit mycard = DIAMONDS;

  if (mycard & (DIAMONDS | HEARTS))
    {
      printf("My card is either a diamonds or a hearts");
    }
  
  return 0;
}
