#include <stdio.h>

void shortints()
{
  short int x = 15213;
  short int y = -15213;

  printf ("%d as positive is %X\n", x, x);
  printf ("%d as negative is %X\n", y, y);
}

void unsignedtosigned()
{
  int tx, ty;
  unsigned ux, uy;

  ty = 5;
  ux = 8;
  
  tx = (int) ux;
  uy = (unsigned) ty;
}

void shiftnegative()
{
  unsigned int x = 0b10100010;
  unsigned int b = x << -2;

  printf("The result of shifting %X a negative amount is %X\n", x, b);

}


int main()
{
  unsignedtosigned();
  shortints();
}

