#include <stdio.h>

struct A { 
  char a; // 1 bytes
  char b; // 1 bytes
  // 2 bytes
  int c; // 4 bytes
};

struct B { // 1 byte
  char a; // 1 byte
};

struct C { // 16 bytes
  long a; // 8 bytes
  char b; // 8 bytes 
};

struct D { // 24 bytes
  struct A a; // 8 bytes
  struct C c; // 16 bytes
  struct A b; 
};

int main()
{
  struct A mystructure;
  mystructure.b = 'h';

  printf("Size of A is %ld\n", sizeof(struct A));
  printf("Size of B is %ld\n", sizeof(struct B));
  printf("Size of C is %ld\n", sizeof(struct C));
  printf("Size of D is %ld\n", sizeof(struct D));
	

}
