#include <stdio.h>
#include <limits.h>

void printUnsigned(unsigned a)
{
  printf("The result of passing is %u\n", a);
}

void otherExample()
{
  int a = -2147483647;
  unsigned b = 2147483648U;
  int c = 2147483647;
  unsigned d = a - 1U;

  
  // Evaluates to true since the numbers are subtracted as signed
  // then converted to unsigned and compared.
  
  printf("%d - 1 == %d evaluates to %d\n",
	 a, b,
	 a - 1 == b);
  
  // Evaluates the subtraction to yield a signed number
  // then doesn't have to convert for comparing them.
  
  printf("%d - 1 < %d evaluates to %d\n",
	 a, c,
	 a-1 < c);

  printf("%d - 1U < %d evaluates to %d\n",
	 d, c,
	 d < c);
 
}

void truncation_example()
{
  int a = INT_MIN;
  short b = a;

  printf("Result of truncating INT_MIN to short: %hd\n", b);

  int c = INT_MAX;
  short d = (short)c;

  printf("Result of truncating INT_MAX to short: %hd\n", d);

  long e = LONG_MIN;
  int f = e;

  printf("Result of truncating LONG_MIN to int: %d\n", f);

  long g = LONG_MAX;
  int h = g;

  printf("Result of truncating LONG_MAX to int: %d\n", h);
  
}
  

int main()
{
  // printUnsigned(-5);
  truncation_example(); 
}
