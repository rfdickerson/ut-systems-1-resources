#include <stdio.h>

union simple {
  int a;
  long b;
  char c;
};


int main()
{
  union simple s1;
  s1.a = 5;
  s1.c = 'a';
  s1.b = 50000*500;
  
  printf("Value of s1.a is %d\n", s1.a);
  printf("Value of s1.b is %ld\n", s1.b);
  printf("Value of s1.c is %c\n", s1.c);
}
