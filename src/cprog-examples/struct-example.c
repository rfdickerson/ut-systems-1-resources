#include <stdio.h>
#include <math.h>

typedef struct
{
  float x; // x position
  float y; // y position

} PointMass;

/**
 * Computes the distance between two point masses
 **/
float compute_distance(PointMass* m1, PointMass* m2)
{
  
  return sqrt(pow(m1->x-m2->x, 2.0f) + pow(m1->y-m2->y, 2.0f)); 
	      
}

/**
   Computs the force between two point bodies
**/
float compute_force(PointMass* m1, PointMass *m2)
{
  float distance = compute_distance(m1, m2);
  
  
  return distance;
}

int main()
{
  PointMass m1, m2;
  m1.y = 4.0f;
  m1.x = 5.0f;

  m2.x = 55.0f;
  m2.y = 67.0f;

  float a = compute_force(&m1, &m2);

  printf("The force between the point masses is %f\n", a);

  return 0;
}
