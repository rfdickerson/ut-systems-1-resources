#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define KSIZE 1024
char kbuf[KSIZE];

#define MSIZE 528

int copy_from_kernel(void *user_dest, int maxlen)
{
  int len = KSIZE < maxlen ? KSIZE : maxlen;
  memcpy(user_dest, kbuf, len);
  return len;
}

int main()
{

  char mybuf[MSIZE];
  
  copy_from_kernel(mybuf, MSIZE);
  
  
  return 0;
}
