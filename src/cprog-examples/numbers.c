/**
 * Name: numbers
 * Description: Numbers shows off some problems with
 * number representations on computers.
 */

#include <stdio.h>

int main()
{
  int c = 50000 * 50000;

  printf("Result is %d\n", c);


  float s1 = (1e20f + -1e20f) + 3.14f;
  printf("Result of float s1 is: %f\n", s1);
  float s2 = 1e20f + (-1e20f + 3.14f);
  printf("Result of float s2 is: %f\n", s2);
  float s3 = (3.14 + 1e20)-1e20;
  printf("Result of float s3 is: %f\n", s3);
  
}
