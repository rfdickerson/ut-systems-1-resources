#include <stdio.h>
#include <stdlib.h>

void example1()
{
  printf("Example 1\n");
  int a = 3;

  int *b = &a;
  int **c = &b;
  int ***d = &c;
  
  printf("The value is %d \n", **c);
  printf("The value is %d \n", ***d);

}

/** 
 *    This example creates a 2D array by first allocating space for
 *    each row, then for each column
 **/
void example2()
{
  printf("Example 2: Allocated 2d array dynamically\n");
  int i, j;
  int **mat;
  const int N = 10, M = 10;
  
  if ((mat = (int**) malloc(N*sizeof(int) * 10)) == NULL)
    {
      fprintf(stderr, "Some error occurred\n");
      return;
    }

  for ( i = 0; i < N; i++)
    {
      mat[i] = (int*)malloc( M*sizeof(int));
    }

  mat[5][5] = 5;

  for (i=0;i<N;i++)
    {
      for (j=0;j<M;j++)
	{
	  printf("Value at %d, %d is %d\n", i, j, mat[i][j]);
	}
    }
	  
  
}

void example3()
{

}

int main()
{
  example1();
  example2();
}
